﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public Round[] rounds;
	public GameObject coButton;
	public GameObject noButton;
	public GameObject nextButton;
	public Text message;
	private int i;

	void Start() {
		i = 0;
		ShowNext();
	}

	void Update() {
	}

	public void OnNext() {
		rounds[i].a.SetActive(true);
		rounds[i].b.SetActive(true);
		if (i > 0) {

			rounds[i - 1].a.SetActive(false);
			rounds[i - 1].b.SetActive(false);
		}

		ShowCoNo();
	}

	public void OnCo() {
		ShowNext();
		if (rounds[i].isSame) {
			message.text = "Correct";
		} 
		else {
			message.text = "Wrong";
		}
		i = i + 1;
	}

	public void OnNo() {
		ShowNext();
		if (!rounds[i].isSame) {
			message.text = "Correct";
		} 
		else {
			message.text = "Wrong";
		}
		i = i + 1;
	}

	void ShowNext() {
		nextButton.SetActive(true);
		coButton.SetActive(false);
		noButton.SetActive(false);

	}

	void ShowCoNo() {
		nextButton.SetActive(false);
		coButton.SetActive(true);
		noButton.SetActive(true);
		message.text = "Are these people co-stars?";
	}
}

[System.Serializable]
public class Round {
	public GameObject a;
	public GameObject b;
	public bool isSame;
}